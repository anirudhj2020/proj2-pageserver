from flask import Flask, render_template, abort


app = Flask(__name__)


@app.errorhandler(404)
def file_not_found(error):
    return render_template("404.html"), 404

@app.errorhandler(403)
def file_forbidden(error):
    return render_template("403.html"), 403


@app.route("/<path:filename>")
def hello(filename):
    if  "//" in filename:
        abort(403)
    elif filename[0] == "." and filename[1] == ".":
        abort(403)
    elif filename[0] == "~":
        abort(403)
    else:
        try:
            return render_template(filename)
        except Exception as error:
            abort(404)
    








if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')


